import { createSlice } from "@reduxjs/toolkit";
import { post, patch } from "./slice";
import { BASE_URL } from "../../utilities/BaseURL";

export const auth = createSlice({
  name: "auth",
  initialState: {
    isLogin: false,
    token: "",
  },
  reducers: {
    startAsync: (state) => {
      state.loading = true;
    },
    stopAsync: (state) => {
      state.loading = false;
    },
    loginSuccess: (state, action) => {
      state.isLogin = true;
      state.token = action.payload;
    },
    logout: (state) => {
      state.isLogin = false;
      state.token = "";
    },
  },
});

export const {
  startAsync,
  stopAsync,
  loginSuccess,
  logout,
  Login,
} = auth.actions;

export default auth.reducer;

// ---------------- ACTION ---------------

// import {errorMessage} from '../../utils';
const defaultBody = null;

export const login = (
  email,
  password,
  mode,
  history,
  LoginError = () => {}
) => (dispatch) => {
  dispatch(startAsync());
  console.log(password, email);
  dispatch(
    post(
      BASE_URL + "/loginfundraiser",
      {
        email: email,
        password: password,
        mode: mode,
      },
      (res) => {
        console.log(res.data);
        dispatch(loginSuccess("gdhdhgdg"));
        history.replace("/app");
        dispatch(stopAsync());
      },
      (err) => {
        dispatch(stopAsync());
        console.log(err.response);
        LoginError();
      },
      () => {}
    )
  );
};

export const RegisterByGoogle = (email, name, history) => (dispatch) => {
  dispatch(startAsync());
  dispatch(
    post(
      BASE_URL + "/registrationfundraiserbygoogle",
      { name: name, email: email },
      (res) => {
        console.log(res.data);
        dispatch(stopAsync());
        dispatch(loginSuccess("gdhdhgdg"));
        history.replace("/app");
      },
      (err) => {
        dispatch(stopAsync());
        console.log(err.response);
      },
      () => {}
    )
  );
};

export const VerifyAccount = (id, actions) => (dispatch) => {
  dispatch(startAsync());
  dispatch(
    patch(
      BASE_URL + "/verifyfundraiser",
      { id: parseInt(id) ? parseInt(id) : "" },
      (res) => {
        console.log(res.data);
        dispatch(stopAsync());
        actions(true);
      },
      (err) => {
        dispatch(stopAsync());
        actions(false);
      },
      () => {}
    )
  );
};

export const RegistAccount = (Name, Email, Password, history, url) => (
  dispatch
) => {
  dispatch(startAsync());
  dispatch(
    post(
      BASE_URL + "/registrationfundraiser",
      { name: Name, email: Email, password: Password },
      (res) => {
        dispatch(stopAsync());
        history.push(url.split("/").slice(0, -1).join("/") + "/success", {
          page: "register",
        });
      },
      (err) => {
        console.log(err.response);
        dispatch(stopAsync());
      },
      () => {}
    )
  );
};

export const ForgetPassword = (Email, history, url) => (dispatch) => {
  dispatch(startAsync());
  dispatch(
    post(
      BASE_URL + "/forgetpasswordfundraiser",
      { email: Email },
      (res) => {
        dispatch(stopAsync());
        history.push(url.split("/").slice(0, -1).join("/") + "/success", {
          page: "forget",
        });
      },
      (err) => {
        console.log(err.response);
        dispatch(stopAsync());
      },
      () => {}
    )
  );
};

export const NewPassword = (Password, Token, history, url) => (dispatch) => {
  console.log("new", Token);
  dispatch(startAsync());
  dispatch(
    post(
      BASE_URL + "/newpasswordfundraiser",
      { password: Password, token: Token },
      (res) => {
        dispatch(stopAsync());
        history.push(url.split("/").slice(0, -2).join("/") + "/success", {
          page: "newPassword",
        });
      },
      (err) => {
        console.log(err.response);
        dispatch(stopAsync());
      },
      () => {}
    )
  );
};

export const Logout = () => (dispatch) => {
  dispatch(logout());
};
