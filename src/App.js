import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import "./style.css"
import user from "./pages/fundraiser/user";
import home from "./pages/fundraiser/app";
import main from "./pages";



const AuthRoute = ({ component: Component, isLogin, ...rest }) => (

  <Route
  {...rest}
  render={props =>
    isLogin ? (
      <Component {...props} />
    ) : (
      <>
       <Redirect
        to={{
          pathname: "/user/login",
          // state: { from: props.location }
        }}
      />
      <Route path="/user" component={user} />
      </>
    )
  }
/>
);

const App = () => {
  const isLogin = useSelector((state) => state.auth.isLogin);

  useEffect(() => {
    console.log("home",isLogin);
  }, [isLogin]);

  return (
      <Router>
        <Switch>
          {/* {isLogin ? (
            <>
              <Route path="/" component={Home} />
              <Route path="/home" component={Home1} />
            </>
          ) : (
            <>
              <Redirect
                to={{
                  pathname: "/user/login",
                }}
              />
              <Route path="/user" component={user} />
            </>
          )} */}
           <AuthRoute path="/app" isLogin={isLogin} component={home} />
          <Route path="/" exact component={main} />
           <Route path="/user" component={user} />
        </Switch>
      </Router>
  );
};

export default App;
