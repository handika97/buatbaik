import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import Axios from "axios";
import { BsEyeSlash, BsEye } from "react-icons/bs";

const Login = ({ match, sidebar, ScrollY }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  console.log("app side", ScrollY);
  return (
    <Fragment>
      <Wrapper ScrollY={window.pageYOffset}>
        <div className={sidebar ? "sidebaractive" : "sidebar"}></div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .sidebar {
    position: fixed;
    box-shadow: 1px 1px 5px black;
    height: 100vh;
    width: 0px;
    transition: 1s;
    margin: -50px 0vw;
  }
  .sidebaractive {
    height: 100vh;
    margin: ${(props) =>props.ScrollY}px
      -100px;
    /* margin: 0px -100px; */
    width: 100px;
    background-color: black;
    box-shadow: 1px 1px 5px black;
    position: fixed;
    transition: ${(props) =>
        props.ScrollY > 0 ?   0: 1}s;
  }
`;
