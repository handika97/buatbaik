import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import Header from "./header";
import Loading from "../Cell/loading";

const UserLayout = (props) => {
  const history = useHistory();
  let { url } = useRouteMatch();
  return (
    <Fragment>
      <Wrapper>
        {props.loading && <Loading />}
        <div className="container">
          <Header />
          <div className="sub-container">
            <div className="containerItems">{props.children}</div>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default UserLayout;

const Wrapper = styled.div`
  .container {
    background-color: #e8e7e6;
    height: 100vh;
    width: 100vw;
    display: flex;
    overflow: hidden;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    box-sizing: border-box;
    .sub-container {
      padding: 30px 20px;
      height: 100%;
      width: 100%;
      box-sizing: border-box;
      max-width: 800px;
      .containerItems {
        background-color: white;
        border-radius: 20px;
        height: 100%;
        width: 100%;
        box-sizing: border-box;
        overflow: auto;
      }
    }
  }
`;
