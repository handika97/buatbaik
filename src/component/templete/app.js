import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import Axios from "axios";
import { BsEyeSlash, BsEye } from "react-icons/bs";
import Header from "./header";
import Sidebar from "./sidebar";

const Login = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [sidebar, setSidebar] = useState(false);
  console.log("app", window.pageYOffset);
  return (
    <Fragment>
      <Wrapper>
        <div className={sidebar ? "container-sidebar" : "container"}>
          <Header sidebar={sidebar} onClick={() => setSidebar(!sidebar)} />
          <Sidebar sidebar={sidebar}/>
          <div className="sub-container">
            <div className="containerItems">{props.children}</div>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .container {
    background-color: #e8e7e6;
    height: 100vh;
    width: 100vw;
    display: flex;
    overflow: hidden;
    flex-direction: column;
    transition: 1s;
  }
  .container-sidebar {
    background-color: #e8e7e6;
    height: 100vh;
    width: 100vw;
    margin: 0px 100px;
    display: flex;
    overflow: hidden;
    flex-direction: column;
    transition: 1s;
    position:fixed;
  }
  .sub-container {
    padding: 30px 20px;
    height: 100%;
    width: 100%;
    box-sizing: border-box;
    .containerItems {
      background-color: white;
      border-radius: 20px;
      height: 100%;
      width: 100%;
      box-sizing: border-box;
      overflow: auto;
    }
  }
`;
