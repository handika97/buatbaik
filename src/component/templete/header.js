import React, { useState, Fragment, useEffect } from "react";
import { useRouteMatch } from "react-router-dom";
import styled from "styled-components";
import { BiMenu, BiUserCircle } from "react-icons/bi";
import { Logout } from "../../redux/features/authSlice";
import { useSelector, useDispatch } from "react-redux";

const Header = ({ match, onClick = () => {}, ScrollY }) => {
  const dispatch = useDispatch();
  const [page, setPage] = useState(false);
  const { url } = useRouteMatch();
  // console.log("header", url.split("/").slice(1, -1));
  useEffect(() => {
    if (url === "/user") {
      setPage(false);
    } else {
      setPage(true);
    }
    console.log("sty", ScrollY);
  }, [url, ScrollY]);
  return (
    // <Fragment>
    <Wrapper ScrollY={ScrollY}>
      <div className={page ? "navbar" : "navbar-user"}>
        {page && <BiMenu size={25} onClick={() => onClick()} />}
        <p className="textHeader">
          SUARA<p className="sub-textHeader">SEMUT.COM</p>
        </p>
        {page && <BiUserCircle size={25} onClick={() => dispatch(Logout())} />}
      </div>
    </Wrapper>
    // </Fragment>
  );
};

export default Header;

const Wrapper = styled.div`
  .navbar-user {
    width: 100vw;
    min-height: 50px;
    height: 50px;
    background-color: white;
    display: flex;
    align-items: center;
    justify-content: center;
    /* position: fixed; */
    /* align-self: flex-start; */
    .textHeader {
      display: flex;
      flex-direction: row;
      color: #ff6600;
      font-size: 15px;
      font-weight: bold;
      .sub-textHeader {
        color: black;
        font-size: 15px;
        font-weight: bold;
      }
    }
  }
  .navbar {
    width: 100vw;
    height: 50px;
    background-color: white;
    display: flex;
    align-items: center;
    margin: ${(props) =>
        props.ScrollY > 50 && props.ScrollY < 100 ? props.ScrollY - 100 : 0}px
      0px;
    position: ${(props) => (props.ScrollY > 50 ? "fixed" : "relative")};
    padding: 0px 20px;
    box-sizing: border-box;
    justify-content: space-between;
    .textHeader {
      display: flex;
      flex-direction: row;
      color: #ff6600;
      font-size: 15px;
      font-weight: bold;
      .sub-textHeader {
        color: black;
        font-size: 15px;
        font-weight: bold;
      }
    }
  }
`;
