import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import Axios from "axios";
import { BsEyeSlash, BsEye } from "react-icons/bs";
import Header from "./header";
import Sidebar from "./sidebar";

const Login = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [sidebar, setSidebar] = useState(false);
  const [scrollY, setScrollY] = useState(0);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      console.log(window.scrollY, window.innerHeight);

      setScrollY(window.scrollY);
    });

    return () => {
      window.removeEventListener("scroll", () => {
        console.log(window.scrollY, window.innerHeight);
      });
    };
  }, []);

  return (
    <Fragment>
      <Wrapper ScrollY={scrollY}>
        <div className={sidebar ? "container-sidebar" : "container"}>
          <Sidebar sidebar={sidebar} ScrollY={scrollY} />
          <Header
            sidebar={sidebar}
            onClick={() => setSidebar(!sidebar)}
            ScrollY={scrollY}
          />
          <div className="sub-container">
            <div className="containerItems">{props.children}</div>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .container {
    background-color: #e8e7e6;
    width: 100vw;
    display: flex;
    overflow: hidden;
    flex-direction: column;
    transition: 1s;
  }
  .container-sidebar {
    margin: 0px 100px;
    background-color: #e8e7e6;
    width: 100vw;
    display: flex;
    overflow: hidden;
    flex-direction: column;
    transition: 1s;
    position: fixed;
  }
  .sub-container {
    padding: 30px 20px;
    min-height: 1000px;
    width: 100%;
    box-sizing: border-box;
    .containerItems {
      background-color: white;
      border-radius: 20px;
      height: 100%;
      margin: ${(props) => (props.ScrollY > 50 ? 50 : 0)}px 0px;
      width: 100%;
      box-sizing: border-box;
      overflow: auto;
    }
  }
`;
