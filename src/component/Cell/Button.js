import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";

import styled, { css } from "styled-components";

const Button = ({ onClick = () => {}, className, Title }) => {
  return (
    <Wrapper className={className}>
      <div className={className} onClick={() => onClick()}>
        <p>{Title}</p>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  ${(props) =>
    props.className === "google" &&
    css`
      .google {
        width: 100%;
        border: 1px solid #757575;
        background-color: white;
        border-radius: 50px;
        height: 40px;
        margin: 5px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: row;
        p {
          font-size: 14px;
          font-weight: 200;
          color: grey;
        }
      }
    `}
  ${(props) =>
    props.className === "orange" &&
    css`
      .orange {
        width: 100%;
        border: 1px solid grey;
        background-color: #ff6600;
        border-radius: 50px;
        height: 40px;
        margin: 10px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        p {
          font-size: 14px;
          font-weight: 200;
          color: white;
        }
      }
    `}
    cursor:pointer;
`;

export default Button;
