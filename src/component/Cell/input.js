import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { BsEyeSlash, BsEye } from "react-icons/bs";

import styled, { css } from "styled-components";

const Input = ({
  onClick = () => {},
  className,
  Title,
  Value,
  onChange = () => {},
  placeholder,
  type,
  password,
  SeePassword,
  Correction,
  correctionText,
}) => {
  console.log(password);
  //   const [SeePassword, setSeePassword] = useState(false);
  return (
    <Wrapper className={className}>
      <div className={className}>
        <input
          placeholder={placeholder}
          type={type}
          value={Value}
          onChange={(e) => onChange(e.target.value)}
          size="0"
        />
        {password ? (
          SeePassword ? (
            <BsEye
              className="icons-password"
              onClick={() => onClick()}
              size={20}
            />
          ) : (
            <BsEyeSlash
              className="icons-password"
              onClick={() => onClick()}
              size={20}
            />
          )
        ) : null}
      </div>
      {!Correction && (
        <p
          style={{
            color: "red",
            fontSize: 12,
            alignSelf: "flex-start",
          }}
        >
          {correctionText}
        </p>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  .input {
    width: 100%;
    margin: 7px 0px;
    border: 1px solid grey;
    border-radius: 5px;
    input {
      height: 40px;
      width: 100%;
      border-radius: 5px;
      padding: 10px;
      border: white;
      box-sizing: border-box;
    }
  }
  .wrong-input {
    width: 100%;
    margin: 10px 0px;
    border: 1px solid red;
    border-radius: 5px;
    input {
      height: 40px;
      border-radius: 5px;
      width: 100%;
      padding: 10px;
      border: white;
      box-sizing: border-box;
    }
  }
  .password {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    box-sizing: border-box;
    .icons-password {
      position: absolute;
      right: 10px;
    }
  }
  input::-ms-reveal,
  input::-ms-clear {
    display: none;
  }
`;

export default Input;

Input.defaulProps = {
  password: false,
};
