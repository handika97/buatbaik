import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import home from "./home";
import AppLayout from "../../../component/templete/AppLayout";

const User = ({ match }) => {
  console.log("user", match);
  return (
    <AppLayout>
      <Switch>
        <Route path={`${match.url}/home`} component={home} />
        <Redirect
          exact
          to={`${match.url}/home`}
        />
      </Switch>
    </AppLayout>
  );
};

export default User;
