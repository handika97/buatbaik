import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import styled from "styled-components";
import Axios from "axios";
import Button from "../../../component/Cell/Button";

const Login = ({ match }) => {
  const history = useHistory();
  let { url } = useRouteMatch();
  const Page = history.location?.state?.page;

  return (
    <Fragment>
      <Wrapper>
        <div className="center">
          <p>
            {Page === "register"
              ? "Verifikasi Email Anda"
              : "Atur Ulang Password Anda"}
          </p>
          <p className="sub-text">
            {Page === "newPassword" ? null : "Cek Email Anda dan Klik Link"}
            {Page === "register" ? "Aktivasi Akun" : "Atur Ulang Password"}
            {Page === "newPassword" ? " Berhasil" : null}
          </p>
          <div className="w-100 max-w200">
            <Button
              className="orange"
              onClick={() =>
                history.push(url.split("/").slice(0, -1).join("/") + "/login")
              }
              Title="Masuk Kembali"
            />
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .center {
    width: 100%;
    min-height: 80vh;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  p {
    font-size: 25px;
    font-weight: bold;
  }
  p.sub-text {
    margin: 20px 0px;
    font-size: 15px;
    font-weight: 100;
  }
`;
