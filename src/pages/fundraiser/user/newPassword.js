import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import Button from "../../../component/Cell/Button";
import { NewPassword } from "../../../redux/features/authSlice";
import { useDispatch } from "react-redux";
import Input from "../../../component/Cell/input";

const Login = ({ match }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  let { token } = useParams();
  console.log(url.split("/").slice(0, -2).join("/") + "/success");
  const [Password, setPassword] = useState("");
  const [RepeatPassword, setRepeatPassword] = useState("");
  const [PasswordStatus, setPasswordStatus] = useState(true);
  const [RepeatPasswordStatus, setRepeatPasswordStatus] = useState(true);

  useEffect(() => {
    if (Password.length > 0) {
      setTimeout(() => {
        if (Password.length < 6) {
          setPasswordStatus(false);
        } else {
          setPasswordStatus(true);
        }
      }, 1000);
    }
    if (RepeatPassword.length > 0) {
      setTimeout(() => {
        if (Password !== RepeatPassword) {
          setRepeatPasswordStatus(false);
        } else {
          setRepeatPasswordStatus(true);
        }
      }, 1000);
    }
  }, [Password, RepeatPassword]);

  const newPassword = () => {
    if (!Password) {
      setPasswordStatus(false);
    } else if (!RepeatPassword) {
      setRepeatPasswordStatus(false);
    } else if (
      RepeatPasswordStatus &&
      PasswordStatus &&
      Password &&
      RepeatPassword
    ) {
      dispatch(NewPassword(Password, token, history, url));
    }
  };
  return (
    <Fragment>
      <Wrapper>
        <div className="center">
          <p>Password Baru</p>
          <div className="w-80">
            <Input
              className={
                (PasswordStatus ? "input" : "wrong-input") + " password"
              }
              type={"Password"}
              placeholder="Password"
              Value={Password}
              onChange={(e) => setPassword(e)}
              Correction={PasswordStatus}
              correctionText={
                Password
                  ? "Password Terlalu Pendek (minimal 6 karakter)"
                  : "Masukan Password"
              }
            />
          </div>
          <div className="w-80">
            <Input
              className={RepeatPasswordStatus ? "input" : "wrong-input"}
              placeholder="Ulangi Password"
              type={"Password"}
              Value={RepeatPassword}
              onChange={(e) => setRepeatPassword(e)}
              Correction={RepeatPasswordStatus}
              correctionText={
                RepeatPassword ? "Password Tidak sama" : "Ulangi Password"
              }
            />
          </div>
          <div className="w-100 max-w200">
            <Button
              className="orange"
              onClick={() => newPassword()}
              Title="Submit"
            />
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  p {
    font-size: 25px;
    font-weight: bold;
  }
  p.sub-text {
    margin: 20px 0px;
    font-size: 15px;
    font-weight: 100;
  }
  .daftar-button {
    width: 100%;
    max-width: 250px;
    border: 1px solid grey;
    background-color: #ff6600;
    border-radius: 50px;
    height: 40px;
    margin: 10px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    p {
      font-size: 14px;
      font-weight: 200;
      color: white;
    }
  }
  .center {
    width: 100%;
    min-height: 80vh;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w-80 {
    width: 80%;
  }
`;
