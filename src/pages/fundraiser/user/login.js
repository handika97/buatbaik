import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { login, RegisterByGoogle } from "../../../redux/features/authSlice";
import styled from "styled-components";
import { auth, provider } from "../../../firebase";
import firebase from "firebase/app";
import Button from "../../../component/Cell/Button";
import Input from "../../../component/Cell/input";

const Login = ({ match }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const { id } = params;
  const [SeePassword, setSeePassword] = useState(false);
  const [Password, setPassword] = useState("");
  const [PasswordStatus, setPasswordStatus] = useState(true);
  const [Email, setEmail] = useState("");
  const [EmailStatus, setEmailStatus] = useState(true);
  const [LoginStatus, setLoginStatus] = useState(true);

  useEffect(() => {
    if (Email.length > 0) {
      setTimeout(() => {
        validateEmail();
      }, 1000);
    }
  }, [Email]);

  function validateEmail() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return setEmailStatus(re.test(String(Email).toLowerCase()));
  }
  useEffect(() => {
    if (Password.length > 0) {
      setTimeout(() => {
        if (Password.length < 6) {
          setPasswordStatus(false);
        } else {
          setPasswordStatus(true);
        }
      }, 1000);
    }
  }, [Password]);

  const LoginGoogle = () => {
    setLoginStatus(true);
    firebase
      .auth()
      .signInWithPopup(provider)
      .then((res) => {
        console.log(res);
        const { isNewUser, profile } = res.additionalUserInfo;
        dispatch(RegisterByGoogle(profile.email, profile.name, history));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const Login = () => {
    if (!Email) {
      setEmailStatus(false);
    } else if (!Password) {
      setPasswordStatus(false);
    } else if (EmailStatus && PasswordStatus && Password && Email) {
      setLoginStatus(true);
      dispatch(
        login(Email, Password, "normally", history, () => {
          setLoginStatus(false);
        })
      );
    }
  };

  return (
    <Fragment>
      <Wrapper>
        <div className="sub-items">
          <div className="title">
            <p>MASUK</p>
            {!LoginStatus && <p className="failed-login">Login Gagal</p>}
          </div>
          <div className="w-100">
            <Button
              className="google"
              onClick={() => LoginGoogle()}
              Title={
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    height: "100%",
                    width: "100%",
                    gap: 10,
                  }}
                >
                  <img
                    src={require("../../../assets/image/googleLogo.png")}
                    style={{ height: 20 }}
                  />
                  <p>Lanjutkan Dengan Google</p>
                </div>
              }
            />
          </div>
          <div className="atau-line">
            <div className="line" />
            <p>ATAU</p>
            <div className="line" />
          </div>
          <div className="w-100">
            <Input
              className={EmailStatus ? "input" : "wrong-input"}
              placeholder="Email"
              type={"email"}
              Value={Email}
              onChange={(e) => setEmail(e)}
              Correction={EmailStatus}
              correctionText={Email ? "Format Email Salah" : "Masukan Email"}
            />
          </div>
          <div className="w-100">
            <Input
              password
              className={
                (PasswordStatus ? "input" : "wrong-input") + " password"
              }
              placeholder="Password"
              type={!SeePassword ? "password" : "text"}
              Value={Password}
              onChange={(e) => setPassword(e)}
              SeePassword={SeePassword}
              onClick={() => setSeePassword(!SeePassword)}
              Correction={PasswordStatus}
              correctionText={
                Password.length
                  ? "Password Terlalu Pendek (minimal 6 karakter)"
                  : "Masukan Password"
              }
            />
          </div>
          <div className="w-100">
            <Button
              className="orange"
              onClick={() => {
                Login();
              }}
              Title="Masuk"
            />
          </div>
          <p
            style={{ margin: 10 }}
            onClick={() =>
              history.push(
                url.split("/").slice(0, -1).join("/") + "/forgetpassword"
              )
            }
          >
            Lupa Kata Sandi?
          </p>
          <div style={{ whiteSpace: "normal" }}>
            <p
              style={{
                fontSize: 15,
                alignSelf: "flex-start",
                color: "grey",
                display: "flex",
                flexDirection: "row",
                whiteSpace: "nowrap",
                marginTop: 20,
              }}
            >
              Belum memiliki akun?{" "}
              <p
                style={{
                  color: "#ff6600",
                  marginLeft: 3,
                  cursor: "pointer",
                }}
                onClick={() =>
                  history.push(
                    url.split("/").slice(0, -1).join("/") + "/registration"
                  )
                }
              >
                Daftar
              </p>
            </p>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .header {
    background-color: #fbfbfb;
    height: 60px;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    border-bottom: 1px solid grey;
    .masukButton {
      color: #ff6600;
      font-weight: bold;
      margin: 7px;
      cursor: pointer;
      :hover {
        border-bottom: 1px solid grey;
      }
    }
  }
  .sub-items {
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 100%;
    padding: 0px 5%;
    box-sizing: border-box;
    .title {
      margin: 50px 0px 20px 0px;
      display: flex;
      width: 100%;
      flex-direction: row;
      justify-content: space-between;
      align-items: flex-end;
      p {
        font-size: 25px;
        font-weight: 500;
        text-align: center;
      }
      p.failed-login {
        font-size: 15px;
        color: red;
      }
    }
    .daftar-button {
      width: 100%;
      border: 1px solid grey;
      background-color: #ff6600;
      border-radius: 50px;
      height: 40px;
      margin: 10px 0px;
      display: flex;
      justify-content: center;
      align-items: center;
      p {
        font-size: 14px;
        font-weight: 200;
        color: white;
      }
    }
    .atau-line {
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: row;
      width: 100%;
      margin: 13px 13px;
      .line {
        width: 40%;
        background-color: #e8e7e6;
        height: 2px;
      }
      p {
        color: #e8e7e6;
        font-size: 15px;
      }
    }
    .w-100 {
      width: 100%;
    }
  }
`;
