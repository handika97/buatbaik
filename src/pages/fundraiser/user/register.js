import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { post } from "../../../redux/features/slice";
import { BASE_URL } from "../../../utilities/BaseURL";
import { provider } from "../../../firebase";
import firebase from "firebase/app";
import {
  login,
  RegisterByGoogle,
  RegistAccount,
} from "../../../redux/features/authSlice";
import Button from "../../../component/Cell/Button";
import Input from "../../../component/Cell/input";

const Login = ({ match }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const [SeePassword, setSeePassword] = useState(false);
  const [Password, setPassword] = useState("");
  const [PasswordStatus, setPasswordStatus] = useState(true);
  const [Email, setEmail] = useState("");
  const [EmailStatus, setEmailStatus] = useState(true);
  const [Name, setName] = useState("");
  const [NameStatus, setNameStatus] = useState(true);

  useEffect(() => {
    if (Email.length > 0) {
      setTimeout(() => {
        validateEmail();
      }, 1000);
    }
    if (Password.length > 0) {
      setTimeout(() => {
        if (Password.length < 6) {
          setPasswordStatus(false);
        } else {
          setPasswordStatus(true);
        }
      }, 1000);
    }
    setNameStatus(true);
  }, [Email, Password, Name]);

  function validateEmail() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return setEmailStatus(re.test(String(Email).toLowerCase()));
  }

  const Register = () => {
    if (!Name) {
      setNameStatus(false);
    } else if (!Email) {
      setEmailStatus(false);
    } else if (!Password) {
      setPasswordStatus(false);
    } else if (EmailStatus && PasswordStatus && Password && Email && Name) {
      dispatch(RegistAccount(Name, Email, Password, history, url));
    }
  };

  const LoginGoogle = () => {
    firebase
      .auth()
      .signInWithPopup(provider)
      .then((res) => {
        console.log(res);
        const { isNewUser, profile } = res.additionalUserInfo;
        console.log();
        dispatch(RegisterByGoogle(profile.email, profile.name, history));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <Fragment>
      <Wrapper>
        <div className="header">
          <p>Sudah memiliki akun?</p>{" "}
          <p
            className="masukButton"
            onClick={() =>
              history.push(url.split("/").slice(0, -1).join("/") + "/login")
            }
          >
            Masuk
          </p>
        </div>
        <div className="sub-items">
          <div className="title">
            <p>Daftar Sebagai Penggalang Dana</p>
          </div>
          <div className="w-100">
            <Input
              className={NameStatus ? "input" : "wrong-input"}
              placeholder="Nama Lengkap"
              type={"text"}
              Value={Name}
              onChange={(e) => setName(e)}
              Correction={NameStatus}
              correctionText="Masukan Nama"
            />
          </div>
          <div className="w-100">
            <Input
              className={EmailStatus ? "input" : "wrong-input"}
              placeholder="Email"
              type={"email"}
              Value={Email}
              onChange={(e) => setEmail(e)}
              Correction={EmailStatus}
              correctionText={Email ? "Format Email Salah" : "Masukan Email"}
            />
          </div>
          <div className="w-100">
            <Input
              password
              className={
                (PasswordStatus ? "input" : "wrong-input") + " password"
              }
              placeholder="Password"
              type={!SeePassword ? "password" : "text"}
              Value={Password}
              onChange={(e) => setPassword(e)}
              SeePassword={SeePassword}
              onClick={() => setSeePassword(!SeePassword)}
              Correction={PasswordStatus}
              correctionText={
                Password
                  ? "Password Terlalu Pendek (minimal 6 karakter)"
                  : "Masukan Password"
              }
            />
          </div>
          <div className="w-100">
            <Button
              className="orange"
              onClick={() => {
                Register();
              }}
              Title="Daftar"
            />
          </div>
          <div
            style={{
              whiteSpace: "normal",
              maxWidth: "80%",
              display: "flex",
              gap: 3,
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            <p
              style={{
                fontSize: 13,
                color: "grey",
                whiteSpace: "nowrap",
              }}
            >
              Dengan mendaftar, saya menyetujui
            </p>
            <p
              style={{
                color: "#ff6600",
                // marginLeft: 3,
                whiteSpace: "nowrap",
              }}
            >
              Syarat dan Ketentuan
            </p>
            <p
              style={{
                fontSize: 13,
                color: "grey",
                whiteSpace: "nowrap",
              }}
            >
              Serta
            </p>
            <p
              style={{
                color: "#ff6600",
                // marginLeft: 3,
                whiteSpace: "nowrap",
              }}
            >
              Kebijakan Privasi
            </p>
          </div>
          <div className="atau-line">
            <div className="line" />
            <p>ATAU</p>
            <div className="line" />
          </div>
          <div className="w-100">
            <Button
              className="google"
              onClick={() => LoginGoogle()}
              Title={
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    height: "100%",
                    width: "100%",
                    gap: 10,
                  }}
                >
                  <img
                    src={require("../../../assets/image/googleLogo.png")}
                    style={{ height: 20 }}
                  />
                  <p>Lanjutkan Dengan Google</p>
                </div>
              }
            />
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .header {
    background-color: #fbfbfb;
    height: 60px;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    border-bottom: 1px solid grey;
    .masukButton {
      color: #ff6600;
      font-weight: bold;
      margin: 7px;
      cursor: pointer;
      :hover {
        border-bottom: 1px solid grey;
      }
    }
  }
  .sub-items {
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 100%;
    padding: 0px 5%;
    box-sizing: border-box;
    .title {
      max-width: 250px;
      margin: 20px 20px;
      p {
        font-size: 25px;
        font-weight: 500;
        text-align: center;
      }
    }

    .atau-line {
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: row;
      width: 100%;
      margin: 7px 13px;
      .line {
        width: 40%;
        background-color: #e8e7e6;
        height: 2px;
      }
      p {
        color: #e8e7e6;
        font-size: 15px;
      }
    }
  }
  .w-100 {
    width: 100%;
  }
`;
