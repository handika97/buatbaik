import React, { useState, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import login from "./login";
import register from "./register";
import registrationSuccess from "./registrationSuccess";
import verifySuccess from "./verifySuccess";
import forgetPassword from "./forgetPassword";
import newPassword from "./newPassword";
import UserLayout from "../../../component/templete/UserLayout";

const User = ({ match }) => {
  console.log("user", match);
  const loading = useSelector((state) => state.auth.loading);

  return (
    <UserLayout loading={loading}>
      <Switch>
        <Route path={`${match.url}/login`} component={login} />
        <Route path={`${match.url}/registration`} component={register} />
        <Route path={`${match.url}/success`} component={registrationSuccess} />
        <Route
          path={`${match.url}/newpassword/:token`}
          component={newPassword}
        />
        <Route
          path={`${match.url}/forgetpassword`}
          component={forgetPassword}
        />
        <Route path={`${match.url}/:id`} component={verifySuccess} />
        {/* <Route path={`${match.url}/success`} component={verification} /> */}
      </Switch>
    </UserLayout>
  );
};

export default User;
