import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import Button from "../../../component/Cell/Button";
import { ForgetPassword } from "../../../redux/features/authSlice";
import { useDispatch } from "react-redux";
import Input from "../../../component/Cell/input";

const Forget_Password = ({ match }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  let { id } = useParams();
  const [status, setStatus] = useState("");
  const [Email, setEmail] = useState("");
  const [EmailStatus, setEmailStatus] = useState(true);

  const forgetPassword = () => {
    if (!Email) {
      setEmailStatus(false);
    } else if (EmailStatus && Email) {
      dispatch(ForgetPassword(Email, history, url));
    }
  };

  return (
    <Fragment>
      <Wrapper>
        <div className="center">
          <p>Masukkan Email Anda</p>
          <div className="w-80">
            <Input
              className={EmailStatus ? "input" : "wrong-input"}
              placeholder="Email"
              type={"email"}
              Value={Email}
              onChange={(e) => setEmail(e)}
              Correction={EmailStatus}
              correctionText={Email ? "Format Email Salah" : "Masukan Email"}
            />
          </div>
          <div className="w-100 max-w200">
            <Button
              className="orange"
              onClick={() => forgetPassword()}
              Title="Submit"
            />
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Forget_Password;

const Wrapper = styled.div`
  p {
    font-size: 25px;
    font-weight: bold;
  }
  p.sub-text {
    margin: 20px 0px;
    font-size: 15px;
    font-weight: 100;
  }
  .daftar-button {
    width: 100%;
    max-width: 250px;
    border: 1px solid grey;
    background-color: #ff6600;
    border-radius: 50px;
    height: 40px;
    margin: 10px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    p {
      font-size: 14px;
      font-weight: 200;
      color: white;
    }
  }
  .center {
    width: 100%;
    min-height: 80vh;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .w-80 {
    width: 80%;
  }
`;
