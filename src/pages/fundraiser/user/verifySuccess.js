import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import Button from "../../../component/Cell/Button";
import { VerifyAccount } from "../../../redux/features/authSlice";
import { useDispatch } from "react-redux";

const Login = ({ match }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  console.log("succes", history.location);
  let { url } = useRouteMatch();
  let { id } = useParams();
  const [status, setStatus] = useState("");
  useEffect(() => {
    dispatch(
      VerifyAccount(id, (e) => {
        console.log("params", e);
        setStatus(e);
      })
    );
  }, []);
  return (
    <Fragment>
      <Wrapper>
        <div className="center">
          <p>Verifikasi Email Anda {status ? "Berhasil" : "Gagal"}</p>
          <p className="sub-text">Silakan Masuk Kembali</p>
          <div className="w-100 max-w200">
            <Button
              className="orange"
              onClick={() =>
                history.push(url.split("/").slice(0, -1).join("/") + "/login")
              }
              Title="Masuk Kembali"
            />
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  p {
    font-size: 25px;
    font-weight: bold;
  }
  p.sub-text {
    margin: 20px 0px;
    font-size: 15px;
    font-weight: 100;
  }
  .daftar-button {
    width: 100%;
    max-width: 250px;
    border: 1px solid grey;
    background-color: #ff6600;
    border-radius: 50px;
    height: 40px;
    margin: 10px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    p {
      font-size: 14px;
      font-weight: 200;
      color: white;
    }
  }
  .center {
    width: 100%;
    min-height: 80vh;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
`;
