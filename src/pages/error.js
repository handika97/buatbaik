import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";


class Error extends Component {
  componentDidMount() {
    document.body.classList.add("background");
  }
  componentWillUnmount() {
    document.body.classList.remove("background");
  }
  render() {
    return (
      <Fragment>
        <div className="fixed-background" />
        <main>
          <div className="container">
                  <div className="position-relative image-side ">
                    <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
                    <p className="white mb-0">Yes, it is indeed!</p>
                  </div>
                  <div className="form-side">
                    <NavLink to={`/`} className="white">
                      <span className="logo-single" />
                    </NavLink>
                      error-title
                    <p className="mb-0 text-muted text-small mb-0">
                      error-code
                    </p>
                    <p className="display-1 font-weight-bold mb-5">404</p>
                    <button
                      href="/app"
                      color="primary"
                      className="btn-shadow"
                      size="lg"
                    >
                      go-back-home
                    </button>
                  </div>
          </div>
        </main>
      </Fragment>
    );
  }
}
export default Error;
